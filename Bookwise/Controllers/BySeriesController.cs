﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookWise.Controllers
{
    public class BySeriesController : Controller
    {
        // GET: BySeries
        public ActionResult ScienceWorld()
        {
            return View();
        }

        public ActionResult GrammarMore()
        {
            return View();
        }

        public ActionResult RightClick()
        {
            return View();
        }

        public ActionResult UrduLikhai()
        {
            return View();
        }

        public ActionResult FunArt()
        {
            return View();
        }

        public ActionResult SocialStudies()
        {
            return View();
        }

        public ActionResult SecondarySocialStudies()
        {
            return View();
        }

        public ActionResult BeginnerSeries()
        {
            return View();
        }

        public ActionResult LetsStartLearning()
        {
            return View();
        }

        public ActionResult MyFunToLearnLanguage()
        {
            return View();
        }

        public ActionResult MyFunToLearnMath()
        {
            return View();
        }
        public ActionResult UrduKiAmliKitab()
        {
            return View();
        }

        public ActionResult AlhusnaIslamyat()
        {
            return View();
        }

    }
}