﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookWise.Models;
using System.IO;
using System.Net;
using System.Net.Mail;
//using System.Web.Mail;

namespace BookWise.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Yellow()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Careers()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Careers(FormCollection fc)
        {
            try
            {
                Careers career = new Careers();
                string numberstostring = "";
                string Filename = "";
                HttpPostedFileBase file0 = Request.Files[0];
                var id = Guid.NewGuid();
                string tempnumber = id.ToString();
                numberstostring = tempnumber.Substring(0, 5);
                if (file0.ContentLength > 0)
                {
                    Filename = numberstostring + "_" + file0.FileName;
                    var path = Path.Combine(Server.MapPath("~/Content/Resumes"), Filename);
                    file0.SaveAs(path);
                    career.CvFile = Filename;
                }
                else
                {
                    career.CvFile = Filename;
                }

                career.Name = fc["Name"];
                career.Email = fc["Email"];
                career.Contact = fc["Contact"];
                career.AppliedAs = fc["Asa"];
                career.SubmittedDate = DateTime.Now.AddHours(12);
                using (BookWiseEntities db = new BookWiseEntities())
                {
                    db.Careers.Add(career);
                    db.SaveChanges();
                }

                //For to send an email at Gmail Starts
                var fromAddress = new MailAddress("marketing@bookwise.com.pk", "Teacher's Corner BookWise");
                var toAddress = new MailAddress("marketing@bookwise.com.pk", "BookWise");
                const string fromPassword = "BW@549900";
                const string subject = "New Career Request";
                const string body = "Dear Admin, You have received an online career request. Please login into your BookWise Admin Portal, to check details. Thank You!";
                var smtp = new SmtpClient
                {
                    Host = "webmail.bookwise.com.pk",
                    Port = 25,
                    //EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                //For to send an email at Gmail Ends
                ViewBag.Color = "green";
                ViewBag.Message = "Thank you for letting us know about your interest. Your request has been submitted successfuly and soon you will be contacted by Bookwise Team!";
            }
            catch (Exception ex)
            {
                ViewBag.Color = "red";
                ViewBag.Message = "Sorry for the inconvenience. Please try again later!";
            }
            return View();
        }

        public ActionResult Distributions()
        {
            return View();
        }

        public ActionResult PriceList()
        {
            return View();
        }

        public ActionResult Sample()
        {
            List<Books> dList = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                dList = db.Books.ToList();
            }
            return View(dList);
        }

        public ActionResult Catelogue()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Catelogue").ToList();
            }
            return View(BookPages);
        }

        [HttpGet]
        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult Orders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Orders(FormCollection fc)
        {
            using (BookWiseEntities dal = new BookWiseEntities())
            {
                Orders order = new Orders();
                order.FullName = fc["fullname"];
                order.SchoolName = fc["schoolname"];
                order.Address = fc["address"];
                order.Contact = fc["contact"];
                order.City = fc["city"];
                order.OrderDate = DateTime.Now.AddHours(12);
                order.TotalAmount = Convert.ToInt32(fc["totalbill"]);
                order.Book1 = Convert.ToInt32(fc["book1"]);
                order.Book2 = Convert.ToInt32(fc["book2"]);
                order.Book3 = Convert.ToInt32(fc["book3"]);
                order.Book4 = Convert.ToInt32(fc["book4"]);
                order.Book5 = Convert.ToInt32(fc["book5"]);
                order.Book6 = Convert.ToInt32(fc["book6"]);
                order.Book7 = Convert.ToInt32(fc["book7"]);
                order.Book8 = Convert.ToInt32(fc["book8"]);
                order.Book9 = Convert.ToInt32(fc["book9"]);
                order.Book10 = Convert.ToInt32(fc["book10"]);
                order.Book11 = Convert.ToInt32(fc["book11"]);
                order.Book12 = Convert.ToInt32(fc["book12"]);
                order.Book13 = Convert.ToInt32(fc["book13"]);
                order.Book14 = Convert.ToInt32(fc["book14"]);
                order.Book15 = Convert.ToInt32(fc["book15"]);
                order.Book16 = Convert.ToInt32(fc["book16"]);
                order.Book17 = Convert.ToInt32(fc["book17"]);
                order.Book18 = Convert.ToInt32(fc["book18"]);
                order.Book19 = Convert.ToInt32(fc["book19"]);
                order.Book20 = Convert.ToInt32(fc["book20"]);
                order.Book21 = Convert.ToInt32(fc["book21"]);
                order.Book22 = Convert.ToInt32(fc["book22"]);
                order.Book23 = Convert.ToInt32(fc["book23"]);
                order.Book24 = Convert.ToInt32(fc["book24"]);
                order.Book25 = Convert.ToInt32(fc["book25"]);
                order.Book26 = Convert.ToInt32(fc["book26"]);
                order.Book27 = Convert.ToInt32(fc["book27"]);
                order.Book28 = Convert.ToInt32(fc["book28"]);
                order.Book29 = Convert.ToInt32(fc["book29"]);
                order.Book30 = Convert.ToInt32(fc["book30"]);
                order.Book31 = Convert.ToInt32(fc["book31"]);
                order.Book32 = Convert.ToInt32(fc["book32"]);
                order.Book33 = Convert.ToInt32(fc["book33"]);
                order.Book34 = Convert.ToInt32(fc["book34"]);
                order.Book35 = Convert.ToInt32(fc["book35"]);
                order.Book36 = Convert.ToInt32(fc["book36"]);
                order.Book37 = Convert.ToInt32(fc["book37"]);
                order.Book38 = Convert.ToInt32(fc["book38"]);
                order.Book39 = Convert.ToInt32(fc["book39"]);
                order.Book40 = Convert.ToInt32(fc["book40"]);
                order.Book41 = Convert.ToInt32(fc["book41"]);
                order.Book42 = Convert.ToInt32(fc["book42"]);
                order.Book43 = Convert.ToInt32(fc["book43"]);
                order.Book44 = Convert.ToInt32(fc["book44"]);
                order.Book45 = Convert.ToInt32(fc["book45"]);
                order.Book46 = Convert.ToInt32(fc["book46"]);
                order.Book47 = Convert.ToInt32(fc["book47"]);
                order.Book48 = Convert.ToInt32(fc["book48"]);
                order.Book49 = Convert.ToInt32(fc["book49"]);
                order.Book50 = Convert.ToInt32(fc["book50"]);
                order.Book51 = Convert.ToInt32(fc["book51"]);
                order.Book52 = Convert.ToInt32(fc["book52"]);
                order.Book53 = Convert.ToInt32(fc["book53"]);
                order.Book54 = Convert.ToInt32(fc["book54"]);
                order.Book55 = Convert.ToInt32(fc["book55"]);
                order.Book56 = Convert.ToInt32(fc["book56"]);
                order.Book57 = Convert.ToInt32(fc["book57"]);
                order.Book58 = Convert.ToInt32(fc["book58"]);
                order.Book59 = Convert.ToInt32(fc["book59"]);
                order.Book60 = Convert.ToInt32(fc["book60"]);
                order.Book61 = Convert.ToInt32(fc["book61"]);
                order.Book62 = Convert.ToInt32(fc["book62"]);
                order.Book63 = Convert.ToInt32(fc["book63"]);
                order.Book64 = Convert.ToInt32(fc["book64"]);
                order.Book65 = Convert.ToInt32(fc["book65"]);
                order.Book66 = Convert.ToInt32(fc["book66"]);
                order.Book67 = Convert.ToInt32(fc["book67"]);
                order.Book68 = Convert.ToInt32(fc["book68"]);
                order.Book69 = Convert.ToInt32(fc["book69"]);
                order.Book70 = Convert.ToInt32(fc["book70"]);
                order.Book71 = Convert.ToInt32(fc["book71"]);
                dal.Orders.Add(order);
                dal.SaveChanges();
                //For to send an email at Gmail Starts
                var fromAddress = new MailAddress("marketing@bookwise.com.pk", "Teacher's Corner BookWise");
                var toAddress = new MailAddress("marketing@bookwise.com.pk", "BookWise");
                const string fromPassword = "BW@549900";
                const string subject = "New Online Order";
                const string body = "Dear Admin, You have received an online order. Please login into your Bookwise Admin Portal, to check details. Thank You!";
                var smtp = new SmtpClient
                {
                    Host = "webmail.bookwise.com.pk",
                    Port = 25,
                    //EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                //For to send an email at Gmail Ends
                TempData["message"] = "Your order has been placed successfully! Soon you will be contacted by Bookwise team member.";
                TempData.Keep("message");
            }
            return RedirectToAction("Orders", "Home");
        }

        [HttpGet]
        public ActionResult SampleOrders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SampleOrders(FormCollection fc)
        {
            try
            {
                using (BookWiseEntities bw = new BookWiseEntities())
                {
                    OrderSamples ordersample = new OrderSamples();
                    ordersample.FullName = fc["fullname"];
                    ordersample.SchoolName = fc["schoolname"];
                    ordersample.Address = fc["address"];
                    ordersample.Contact = fc["contact"];
                    ordersample.City = fc["city"];
                    ordersample.OrderDate = DateTime.Now.AddHours(12);
                    ordersample.Book1 = (fc["book1"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book2 = (fc["book2"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book3 = (fc["book3"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book4 = (fc["book4"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book5 = (fc["book5"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book6 = (fc["book6"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book7 = (fc["book7"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book8 = (fc["book8"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book9 = (fc["book9"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book10 = (fc["book10"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book11 = (fc["book11"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book12 = (fc["book12"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book13 = (fc["book13"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book14 = (fc["book14"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book15 = (fc["book15"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book16 = (fc["book16"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book17 = (fc["book17"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book18 = (fc["book18"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book19 = (fc["book19"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book20 = (fc["book20"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book21 = (fc["book21"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book22 = (fc["book22"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book23 = (fc["book23"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book24 = (fc["book24"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book25 = (fc["book25"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book26 = (fc["book26"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book27 = (fc["book27"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book28 = (fc["book28"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book29 = (fc["book29"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book30 = (fc["book30"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book31 = (fc["book31"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book32 = (fc["book32"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book33 = (fc["book33"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book34 = (fc["book34"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book35 = (fc["book35"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book36 = (fc["book36"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book37 = (fc["book37"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book38 = (fc["book38"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book39 = (fc["book39"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book40 = (fc["book40"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book41 = (fc["book41"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book42 = (fc["book42"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book43 = (fc["book43"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book44 = (fc["book44"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book45 = (fc["book45"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book46 = (fc["book46"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book47 = (fc["book47"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book48 = (fc["book48"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book49 = (fc["book49"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book50 = (fc["book50"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book51 = (fc["book51"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book52 = (fc["book52"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book53 = (fc["book53"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book54 = (fc["book54"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book55 = (fc["book55"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book56 = (fc["book56"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book57 = (fc["book57"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book58 = (fc["book58"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book59 = (fc["book59"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book60 = (fc["book60"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book61 = (fc["book61"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book62 = (fc["book62"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book63 = (fc["book63"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book64 = (fc["book64"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book65 = (fc["book65"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book66 = (fc["book66"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book67 = (fc["book67"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book68 = (fc["book68"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book69 = (fc["book69"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book70 = (fc["book70"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    ordersample.Book71 = (fc["book71"] ?? "").Equals("on", StringComparison.CurrentCultureIgnoreCase);
                    bw.OrderSamples.Add(ordersample);
                    bw.SaveChanges();
                    //For to send an email at Gmail Starts
                    var fromAddress = new MailAddress("marketing@bookwise.com.pk", "Teacher's Corner BookWise");
                    var toAddress = new MailAddress("marketing@bookwise.com.pk", "BookWise");
                    const string fromPassword = "BW@549900";
                    const string subject = "New Sample Order";
                    const string body = "Dear Admin, You have received an online sample order. Please login into your Bookwise Admin Portal, to check details. Thank You!";
                    var smtp = new SmtpClient
                    {
                        Host = "webmail.bookwise.com.pk",
                        Port = 25,
                        //EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 20000
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                    TempData["message"] = "Your order has been placed successfully! Soon you will be contacted by Bookwise staff.";
                    TempData.Keep("message");
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = "Sorry! Please try again later!";
                TempData.Keep("message");
            }
            return RedirectToAction("SampleOrders", "Home");
        }

        public ActionResult test()
        {
            return View();
        }
    }
}