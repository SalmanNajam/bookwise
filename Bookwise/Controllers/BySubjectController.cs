﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookWise.Controllers
{
    public class BySubjectController : Controller
    {
        // GET: BySubject
        public ActionResult English()
        {
            return View();
        }

        public ActionResult Urdu()
        {
            return View();
        }

        public ActionResult Math()
        {
            return View();
        }

        public ActionResult ComputerScience()
        {
            return View();
        }

        public ActionResult Science()
        {
            return View();
        }

        public ActionResult Islamiyat()
        {
            return View();
        }

        public ActionResult SocialStudies()
        {
            return View();
        }

        public ActionResult ArtCraft()
        {
            return View();
        }
    }
}