﻿using BookWise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookWise.Controllers
{
    public class EBookController : Controller
    {

        //public ActionResult AddQuickBooks1()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 20; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  1-" + i + " copy.jpg";
        //            book.Category = "RightClick1";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks2()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 18; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  2-" + i + " copy.jpg";
        //            book.Category = "RightClick2";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks3()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 22; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  3-" + i + " copy.jpg";
        //            book.Category = "RightClick3";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks4()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 28; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  4-" + i + " copy.jpg";
        //            book.Category = "RightClick4";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks5()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 28; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  5-" + i + " copy.jpg";
        //            book.Category = "RightClick5";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks6()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 50; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  6-" + i + " copy.jpg";
        //            book.Category = "RightClick6";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks7()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 55; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  7-" + i + " copy.jpg";
        //            book.Category = "RightClick7";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}

        //public ActionResult AddQuickBooks8()
        //{
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        for (int i = 2; i <= 64; i++)
        //        {
        //            Books book = new Books();
        //            book.Name = "Right Click  8-" + i + " copy.jpg";
        //            book.Category = "RightClick8";
        //            db.Books.Add(book);
        //            db.SaveChanges();
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //}


        // GET: EBook
        //public ActionResult ScienceBook1()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "ScienceBook1").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult ScienceBook2()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "ScienceBook2").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult ScienceBook3()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "ScienceBook3").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult ScienceBook4()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "ScienceBook4").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult ScienceBook5()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "ScienceBook5").ToList();
        //    }
        //    return View(BookPages);
        //}

        public ActionResult AlifBayPay()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "AlifBayPay").ToList();
            }
            return View(BookPages);
        }

        public ActionResult AlphabetBook()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "AlphabatBook").ToList();
            }
            return View(BookPages);
        }

        public ActionResult AlphabatActivityBookCapital()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "AlphabatActivityBookCapital").ToList();
            }
            return View(BookPages);
        }

        public ActionResult AlphabatActivityBookSmall()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "AlphabatActivityBookSmall").ToList();
            }
            return View(BookPages);
        }

        public ActionResult CountingActivityBook()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "CountingActivityBook").ToList();
            }
            return View(BookPages);
        }

        public ActionResult CountingBook()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "CountingBook").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhayOrRangBharein()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhayOrRangBharein").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhaiAwal()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhaiAwal").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhaiCharam()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhaiCharam").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhaiDoom()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhaiDoom").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhaiIbtaai()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhaiIbtaai").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhaiSooom()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhaiSooom").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduKiAmliKitabIbtadiAwal()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduKiAmliKitabIbtadiAwal").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduKiAmliKitabIbtadiDoom()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduKiAmliKitabIbtadiDoom").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduKiAmliKitabIbtadiSoom()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduKiAmliKitabIbtadiSoom").ToList();
            }
            return View(BookPages);
        }

        public ActionResult SocialStudy6()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "SocialStudy6").ToList();
            }
            return View(BookPages);
        }

        public ActionResult SocialStudy7()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "SocialStudy7").ToList();
            }
            return View(BookPages);
        }

        public ActionResult SocialStudy8()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "SocialStudy8").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ScienceBook1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ScienceBook1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ScienceBook2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ScienceBook2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ScienceBook3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ScienceBook3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ScienceBook4()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ScienceBook4").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ScienceBook5()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ScienceBook5").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick4()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick4").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick5()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick5").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick6()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick6").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick7()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick7").ToList();
            }
            return View(BookPages);
        }

        public ActionResult RightClick8()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "RightClick8").ToList();
            }
            return View(BookPages);
        }

        public ActionResult MyFunToLearnMath1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "MyFunToLearnMath1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult MyFunToLearnMath2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "MyFunToLearnMath2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult MyFunToLearnMath3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "MyFunToLearnMath3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult MyFunToLearnLanguage1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "MyFunToLearnLanguage1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult MyFunToLearnLanguage2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "MyFunToLearnLanguage2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult MyFunToLearnLanguage3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "MyFunToLearnLanguage3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ActivityBookLSL()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "123ActivityBookLSL").ToList();
            }
            return View(BookPages);
        }

        public ActionResult PictureBookLSL()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "123PictureBookLSL").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ABCActivityBookLSL()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ABCActivityBookLSL").ToList();
            }
            return View(BookPages);
        }

        public ActionResult ABCPictureBookLSL()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "ABCPictureBookLSL").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduLikhayRangLSL()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduLikhayRangLSL").ToList();
            }
            return View(BookPages);
        }

        public ActionResult UrduTasveriQuiadaLSL()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "UrduTasveriQuiadaLSL").ToList();
            }
            return View(BookPages);
        }

        public ActionResult IslamiyatBook1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "IslamiyatBook1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult IslamiyatBook2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "IslamiyatBook2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult IslamiyatBook3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "IslamiyatBook3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult IslamiyatBook4()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "IslamiyatBook4").ToList();
            }
            return View(BookPages);
        }

        public ActionResult IslamiyatBook5()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "IslamiyatBook5").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore4()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar4").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore5()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar5").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore6()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar6").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore7()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar7").ToList();
            }
            return View(BookPages);
        }

        public ActionResult GrammarMore8()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "Grammar8").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraft1()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraft1").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraft2()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraft2").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraft3()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraft3").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraft4()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraft4").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraft5()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraft5").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraftA()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraftA").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraftB()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraftB").ToList();
            }
            return View(BookPages);
        }

        public ActionResult FunArtCraftC()
        {
            List<Books> BookPages = new List<Books>();
            using (BookWiseEntities db = new BookWiseEntities())
            {
                BookPages = db.Books.Where(x => x.Category == "FunArtCraftC").ToList();
            }
            return View(BookPages);
        }


        public ActionResult QuickView(int id)
        {
            using (BookWiseEntities dal = new BookWiseEntities())
            {
                BookQuickView bqv = (from b in dal.BookQuickView where b.Id == id select b).FirstOrDefault();
                return PartialView("~/Views/Ebook/_QuickView.cshtml", bqv);
            }
        }
    }
}