﻿using BookWise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace BookWise.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        //public ActionResult TeachersCorner(FormCollection fc)
        //{
        //    string actionname = fc["Controller"];
        //    try
        //    {
        //        using (BookWiseEntities db = new BookWiseEntities())
        //        {
        //            TeachersCorner tc = new TeachersCorner();
        //            tc.Name = fc["Name"];
        //            tc.Email = fc["Email"];
        //            tc.ContactNo = fc["Contact"];
        //            tc.SchoolName = fc["SchoolName"];
        //            tc.SchoolAddress = fc["SchoolAddress"];
        //            tc.SubmittedDate = DateTime.Now.AddHours(12);
        //            tc.Status = true;
        //            db.TeachersCorner.Add(tc);
        //            db.SaveChanges();
        //            TempData["Color"] = "green";
        //            TempData["Message"] = "Thank you for submitting your request for login credentials. Soon you will be contacted by BookWise Team!";
        //            TempData.Keep("Color");
        //            TempData.Keep("Message");
        //        }
        //        //For to send an email at Gmail Starts
        //        var fromAddress = new MailAddress("marketing@bookwise.com.pk", "Teacher's Corner BookWise");
        //        var toAddress = new MailAddress("marketing@bookwise.com.pk", "BookWise");
        //        const string fromPassword = "BW@549900";
        //        const string subject = "New Teacher's Request Received";
        //        const string body = "Dear Admin, You have received a new Teacher's login request. Please login into your BookWise Admin Portal, to check this request. Thank You!";
        //        var smtp = new SmtpClient
        //        {
        //            Host = "webmail.bookwise.com.pk",
        //            Port = 25,
        //            //EnableSsl = true,
        //            DeliveryMethod = SmtpDeliveryMethod.Network,
        //            Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
        //            Timeout = 20000
        //        };
        //        using (var message = new MailMessage(fromAddress, toAddress)
        //        {
        //            Subject = subject,
        //            Body = body
        //        })
        //        {
        //            smtp.Send(message);
        //        }
        //        //For to send an email at Gmail Ends
        //        return RedirectToAction("Login", "TeachersCorner");
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["Color"] = "red";
        //        TempData["Message"] = "Please try again later!";
        //        TempData.Keep("Color");
        //        TempData.Keep("Message");
        //        return RedirectToAction(actionname, "TeachersCorner");
        //    }
        //}

        public ActionResult BWAdmin()
        {
            if (Session["Admin"] != null)
            {
                List<TeachersCorner> tcList = new List<TeachersCorner>();
                using (BookWiseEntities dal = new BookWiseEntities())
                {
                    tcList = (from tc in dal.TeachersCorner orderby tc.SubmittedDate descending select tc).ToList();
                }
                return View(tcList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Careers()
        {
            if (Session["Admin"] != null)
            {
                List<Careers> careersList = new List<Models.Careers>();
                using (BookWiseEntities dal = new BookWiseEntities())
                {
                    careersList = (from c in dal.Careers orderby c.SubmittedDate descending select c).ToList();
                }
                return View(careersList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult OnlineOrders()
        {
            if (Session["Admin"] != null)
            {
                try
                {
                    using (BookWiseEntities dal = new BookWiseEntities())
                    {
                        List<Orders> model = (from o in dal.Orders orderby o.OrderDate descending select o).ToList();
                        return View(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult SampleOrders()
        {
            if (Session["Admin"] != null)
            {
                try
                {
                    using (BookWiseEntities dal = new BookWiseEntities())
                    {
                        List<OrderSamples> model = (from o in dal.OrderSamples orderby o.OrderDate descending select o).ToList();
                        return View(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult SampleOrderDetails(int id)
        {
            if (Session["Admin"] != null)
            {
                try
                {
                    using (BookWiseEntities dal = new BookWiseEntities())
                    {
                        ViewBag.BooksNames = (from b in dal.BooksNames select b).ToList();
                        OrderSamples order = (from o in dal.OrderSamples where o.Id == id select o).FirstOrDefault();
                        return PartialView("~/Views/Admin/_SampleOrderDetails.cshtml", order);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult OrderDetails(int id)
        {
            if (Session["Admin"] != null)
            {
                try
                {
                    using (BookWiseEntities dal = new BookWiseEntities())
                    {
                        ViewBag.BooksNames = (from b in dal.BooksNames select b).ToList();
                        Orders order = (from o in dal.Orders where o.Id == id select o).FirstOrDefault();
                        return PartialView("~/Views/Admin/_OrderDetails.cshtml", order);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult CreateTeacherLogin(int id)
        {
            if (Session["Admin"] != null)
            {
                TeachersCorner tc = new TeachersCorner();
                using (BookWiseEntities dal = new BookWiseEntities())
                {
                    tc = (from t in dal.TeachersCorner where t.Id == id select t).FirstOrDefault();
                }
                return View(tc);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult TeacherLogin(FormCollection fc)
        {
            int loginid = Convert.ToInt32(fc["LoginId"]);
            using (BookWiseEntities dal = new BookWiseEntities())
            {
                TeachersCorner tc = (from t in dal.TeachersCorner where t.Id == loginid select t).FirstOrDefault();
                tc.Name = fc["Name"];
                tc.Username = fc["Username"];
                tc.Password = fc["Password"];
                tc.Email = fc["Email"];
                tc.ContactNo = fc["Contact"];
                tc.SchoolName = fc["SchoolName"];
                tc.SchoolAddress = fc["SchoolAddress"];
                tc.CreatedDate = DateTime.Now.AddHours(5);
                tc.Status = false;
                dal.SaveChanges();
            }
            return RedirectToAction("BWAdmin", "Admin");
        }

        public FileResult Download(string filepath, string filename)
        {
            string path = Server.MapPath(filepath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = filename;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection fc)
        {
            try
            {
                TeachersCorner tc = new TeachersCorner();
                using (BookWiseEntities dal = new BookWiseEntities())
                {
                    string username = fc["Username"];
                    string password = fc["Password"];

                    if (username == "bwadmin" && password == "admin12345")
                    {
                        Session["Admin"] = "Admin";
                        return RedirectToAction("BWAdmin", "Admin");
                    }
                    else
                    {
                        TempData["Color"] = "red";
                        TempData["Message"] = "Provided credentials are incorrect!";
                        TempData.Keep("Color");
                        TempData.Keep("Message");
                        return RedirectToAction("Login", "Admin");
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Color"] = "red";
                TempData["Message"] = "Sorry for the inconvenience. Please try again later!";
                TempData.Keep("Color");
                TempData.Keep("Message");
                return RedirectToAction("Login", "TeachersCorner");
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}