﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookWise.Controllers
{
    public class ByLevelController : Controller
    {
        // GET: ByLevel
        public ActionResult PlayGroup()
        {
            return View();
        }
        
        public ActionResult Nursery()
        {
            return View();
        }

        public ActionResult KinderGarten()
        {
            return View();
        }

        public ActionResult Class1()
        {
            return View();
        }

        public ActionResult Class2()
        {
            return View();
        }

        public ActionResult Class3()
        {
            return View();
        }

        public ActionResult Class4()
        {
            return View();
        }

        public ActionResult Class5()
        {
            return View();
        }

        public ActionResult Class6()
        {
            return View();
        }

        public ActionResult Class7()
        {
            return View();
        }

        public ActionResult Class8()
        {
            return View();
        }

        public ActionResult OLevel()
        {
            return View();
        }
    }
}