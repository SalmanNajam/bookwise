﻿using BookWise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookWise.Controllers
{
    public class TeachersCornerController : Controller
    {
        // GET: TeachersCorner
        public ActionResult ScienceWorld()
        {
            return View();
        }

        public ActionResult Grammar()
        {
            return View();
        }

        //public ActionResult GrammarBook1()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x=> x.Category == "GrammarBook1").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult GrammarBook2()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "GrammarBook2").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult GrammarBook3()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "GrammarBook3").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult GrammarBook4()
        //{
        //    return View();
        //}

        //public ActionResult GrammarBook5()
        //{
        //    return View();
        //}

        //public ActionResult GrammarBook6()
        //{
        //    return View();
        //}

        //public ActionResult GrammarBook7()
        //{
        //    return View();
        //}

        //public ActionResult GrammarBook8()
        //{
        //    return View();
        //}

        public ActionResult RightClick()
        {
            return View();
        }

        //public ActionResult RightClickBook1()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "RightClickBook1").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult RightClickBook2()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "RightClickBook2").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult RightClickBook3()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "RightClickBook3").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult RightClickBook4()
        //{
        //    return View();
        //}

        //public ActionResult RightClickBook5()
        //{
        //    return View();
        //}

        public ActionResult SocialStudies()
        {
            return View();
        }

        //public ActionResult SocialStudiesBook6()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "SocialStudiesBook6").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult SocialStudiesBook7()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "SocialStudiesBook7").ToList();
        //    }
        //    return View(BookPages);
        //}

        //public ActionResult SocialStudiesBook8()
        //{
        //    List<Books> BookPages = new List<Books>();
        //    using (BookWiseEntities db = new BookWiseEntities())
        //    {
        //        BookPages = db.Books.Where(x => x.Category == "SocialStudiesBook8").ToList();
        //    }
        //    return View(BookPages);
        //}

        //[HttpGet]
        //public ActionResult Login()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Login(FormCollection fc)
        //{
        //    try
        //    {
        //        TeachersCorner tc = new TeachersCorner();
        //        using (BookWiseEntities dal = new BookWiseEntities())
        //        {
        //            string username = fc["Username"];
        //            string password = fc["Password"];

        //            tc = (from t in dal.TeachersCorner where t.Username == username && t.Password == password select t).FirstOrDefault();
        //            if (tc != null)
        //            {
        //                Session["Teacher"] = tc;
        //                return RedirectToAction("Teachers", "TeachersCorner");
        //            }
        //            else
        //            {
        //                TempData["Color"] = "red";
        //                TempData["Message"] = "Provided username and password is incorrect. Contact BookWise Administrator!";
        //                TempData.Keep("Color");
        //                TempData.Keep("Message");
        //                return RedirectToAction("Login", "TeachersCorner");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["Color"] = "red";
        //        TempData["Message"] = "Sorry for the inconvenience. Please try again later!";
        //        TempData.Keep("Color");
        //        TempData.Keep("Message");
        //        return RedirectToAction("Login", "TeachersCorner");
        //    }
        //}

        //[HttpGet]
        //public ActionResult Teachers()
        //{
        //    if (Session["Teacher"] != null)
        //    {
        //        TeachersCorner tc = (TeachersCorner)Session["Teacher"];
        //        return View(tc);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }

        //}

        //[HttpGet]
        //public ActionResult Logout()
        //{
        //    Session.Abandon();
        //    return RedirectToAction("Index", "Home");
        //}
    }
}