/**
 * The Javascript for our theme.
 *
 * @package VG Skybook
 */
(function($) {
	"use strict"; 
	jQuery(document).ready(function($) {

		// Off Canvas Navigation ---------------------------------------
		var offcanvas_open = false;
		var offcanvas_from_left = false;
		var offcanvas_from_right = false;
		var window_width = $(window).innerWidth();
		
		// Submenu adjustments ---------------------------------------
		function submenu_adjustments() {
			$(".main-navigation > ul > .menu-item").mouseenter(function() {
				if($(this).children(".sub-menu").length > 0) {
					
					var submenu = $(this).children(".sub-menu");
					var window_width = parseInt($(window).outerWidth());
					var submenu_width = parseInt(submenu.outerWidth());
					var submenu_offset_left = parseInt(submenu.offset().left);
					var submenu_adjust = window_width - submenu_width - submenu_offset_left;
					var dir = $('html').attr("dir");
					if(dir == "rtl"){
						var submenu_offset_right = parseInt(submenu.offset().right);
						var submenu_adjust = window_width - submenu_width - submenu_offset_right;
						if(submenu_adjust < 0) {
							alert(submenu);
							submenu.css("right", submenu_adjust-30 + "px");
							submenu.addClass("active");
						}
					}else{
						if(submenu_adjust < 0) {
							submenu.css("left", submenu_adjust-30 + "px");
						}
					}
				}
			});
		}
		
		submenu_adjustments();
		
		function offcanvas_left() {
			$(".vg-website-wrapper").removeClass("slide-from-right");
			$(".vg-website-wrapper").addClass("slide-from-left");
			$(".vg-website-wrapper").addClass("vg-menu-open");
			
			offcanvas_open = true;
			offcanvas_from_left = true;
			
			$(".vg-menu").addClass("open");
			$("body").addClass("offcanvas_open offcanvas_from_left");
			
			$(".nano").nanoScroller();
		}
		
		function offcanvas_right() {
			$(".vg-website-wrapper").removeClass("slide-from-left");
			$(".vg-website-wrapper").addClass("slide-from-right");
			$(".vg-website-wrapper").addClass("vg-menu-open");		
			
			offcanvas_open = true;
			offcanvas_from_right = true;
			
			$(".vg-menu").addClass("open");
			$("body").addClass("offcanvas_open offcanvas_from_right");

			$(".nano").nanoScroller();
		}
		
		function offcanvas_close() {
			if(offcanvas_open === true) {	
				$(".vg-website-wrapper").removeClass("slide-from-left");
				$(".vg-website-wrapper").removeClass("slide-from-right");
				$(".vg-website-wrapper").removeClass("vg-menu-open");
				
				offcanvas_open = false;
				offcanvas_from_left = false;
				offcanvas_from_right = false;
							
				$(".vg-menu").removeClass("open");
				$("body").removeClass("offcanvas_open offcanvas_from_left offcanvas_from_right");
			}
		}
		
		$(".offcanvas-menu-button.offcanvas-right, .open-offcanvas").click(function() {
			offcanvas_right();
		});
		
		$(".offcanvas-menu-button.offcanvas-left").click(function() {
			offcanvas_left();
		});
		
		$(".vg-website-wrapper").on("click", ".vg-pusher-after", function(e) {
			offcanvas_close();
		});
		
		$(".vg-pusher-after").swipe({
			swipeLeft:function(event, direction, distance, duration, fingerCount) {
				offcanvas_close();
			},
			swipeRight:function(event, direction, distance, duration, fingerCount) {
				offcanvas_close();
			},
			tap:function(event, direction, distance, duration, fingerCount) {
				offcanvas_close();
			},
			threshold:0
		});
		
		// Mobile menu ---------------------------------------
		$(".mobile-navigation .menu-item-has-children").append('<div class="more"><i class="fa fa-plus-square"></i></div>');
		
		$(".mobile-navigation").on("click", ".more", function(e) {
			e.stopPropagation();
			
			$(this).parent().toggleClass("current")
				.children(".sub-menu").slideToggle();
							
			$(this).html($(this).html() == '<i class="fa fa-plus-square"></i>' ? '<i class="fa fa-minus-square"></i>' : '<i class="fa fa-plus-square"></i>');
			$(".nano").nanoScroller();
		});
		
		$(".mobile-navigation").on("click", "a", function(e) {
			if(($(this).attr('href') === "#") ||($(this).attr('href') === "")) {
				$(this).parent().children(".more").trigger("click");
			} else {
				offcanvas_close();
			}
		});
		
		// Toogle Search Box ---------------------------------------
		$(".sdropdow-caption").on("click", function(e) {
			$(".sdropdow-content").slideToggle("show");
			$('.sdropdow-caption .fa').toggleClass("fa-close");
			$('.sdropdow-caption .fa').toggleClass("fa-search");
		});
		
		var product_search  = $(".vina-product-search");
		$(".click-search").on("click", function(e) {
			e.stopPropagation();
			
			if(product_search.hasClass("active"))
				product_search.removeClass("active");
			else
				product_search.addClass("active");
			
			e.preventDefault();
		});
		
		$(product_search).on("click", function(e){
			e.stopPropagation();
		});
		
		$(document).on("click", function(e) {
			e.stopPropagation();
			
			if(product_search.hasClass('active'))
				product_search.removeClass('active');
		});
		
		$(".widget_wysija_cont .wysija-submit").wrap('<span class="wysija-submit-wrap"></span>');
		
		
		// Quickview JS ---------------------------------------
		function product_quick_view_ajax(id) 
		{
			$.ajax({
				url: vg_skybook_ajaxurl,
				data: {
					"action" 	 : "vg_skybook_product_quick_view",
					"product_id" : id
				},
				success: function(results) {
					
					$("#placeholder_product_quick_view").html(results);

					var curent_dragging_item;
			
					$("#placeholder_product_quick_view .featured_img_temp").hide();
					
					$("#placeholder_product_quick_view .thumbnails").owlCarousel({
						singleItem : false,
						autoHeight : false,
						transitionStyle:"fade",
						lazyLoad : true,
						slideSpeed : 300,
						dragBeforeAnimFinish: false,
						navigation: true,
						pagination: false,
						navigationText : [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
					});

					$("#quick_view_container").show();
					
					var form_variation = $("#placeholder_product_quick_view").find('.variations_form');
					var form_variation_select = $("#placeholder_product_quick_view").find('.variations_form .variations select');
					
					form_variation.wc_variation_form();
					form_variation_select.change();
				},
				error: function(errorThrown) { console.log(errorThrown); }
			});
		}
		
		$(document).on('click', '.vg_skybook_product_quick_view_button', function(e) {
			e.preventDefault();
			var product_id = $(this).data('product_id');
			product_quick_view_ajax(product_id);
		});
		
		$(window).mouseup(function(e) {	    
			var container = $("#placeholder_product_quick_view");
			if(! container.is(e.target) && container.has(e.target).length === 0) {	    
				$('#quick_view_container').hide();
			}
		});

		$(document).on("click", "#close_quickview", function(){
			$('#quick_view_container').hide();
		});

		$("#quick_view_container").on('click', '.zoom', function(e){
			e.preventDefault();
		});
		
		/* For add to card button */
		$('body').append('<div class="atc-notice-wrapper"><div class="atc-notice"></div><div class="close"><i class="fa fa-times-circle"></i></div></div>');
		
		$('.atc-notice-wrapper .close').on("click", function(){
			$('.atc-notice-wrapper').fadeOut();
			$('.atc-notice').html('');
		});
		
		$('body').on('adding_to_cart', function(event, button, data) {
			var ajaxPId = button.attr('data-product_id');
			//get product info by ajax
			$.post(
				vg_skybook_ajaxurl, 
				{
					'action': 'vg_skybook_get_productinfo',
					'product_id':  ajaxPId
				},
				function(response){
					$('.atc-notice').html(response);
					$(".lazy").lazy();
				}
			);
		});
		
		$('body').on('added_to_cart', function(event, fragments, cart_hash) {			
			$('.atc-notice-wrapper').fadeIn();
		});
		
		// Countdown
		$('.timer-grid').each(function(){
			var countTime = $(this).attr('data-time');
			$(this).countdown(countTime, function(event) {
				$(this).html(
					'<div class="day"><span class="number">'+event.strftime('%D')+' </span><span class="string">days</span></div><div class="timer-driver">:</div><div class="hour"><span class="number">'+event.strftime('%H')+'</span><span class="string">Hrs</span></div><div class="timer-driver">:</div><div class="min"><span class="number">'+event.strftime('%M')+'</span> <span class="string">Mins</sapn></div><div class="timer-driver">:</div><div class="sec"><span class="number">'+event.strftime('%S')+' </span><span class="string">Secs</span></div>'
				);
			});
		});
		
		$('.timer-grid-2').each(function(){
			var countTime = $(this).attr('data-time');
			$(this).countdown(countTime, function(event) {
				$(this).html(
					'<div class="day"><span class="string">days</span><span class="number">'+event.strftime('%D')+' </span></div><div class="timer-driver">:</div><div class="hour"><span class="string">Hrs</span><span class="number">'+event.strftime('%H')+'</span></div><div class="timer-driver">:</div><div class="min"><span class="string">Mins</sapn><span class="number">'+event.strftime('%M')+'</span> </div><div class="timer-driver">:</div><div class="sec"><span class="string">Secs</span><span class="number">'+event.strftime('%S')+' </span></div>'
				);
			});
		});
		
		//Max Menu Category
		$(".category-inside .wgtitle").on("click", function(e) {
			$(".category-inside .vg-emodern-category-treeview ").slideToggle("show");
		});
		
		//Woocommerce
		
		/* Category Product View Module */
		$('.view-mode').each(function(){
			/* Grid View */
			$(this).find('.grid').on("click", function(event){
				event.preventDefault();
				
				$('#content .view-mode').find('.grid').addClass('active');
				$('#content .view-mode').find('.list').removeClass('active');
				
				$('#content .shop-products').removeClass('list-view');
				$('#content .shop-products').addClass('grid-view');
				
				$('#content .list-col4').removeClass('col-xs-12 col-sm-6 col-lg-4');
				$('#content .list-col8').removeClass('col-xs-12 col-sm-6 col-lg-8');
			});
			
			/* List View */
			$(this).find('.list').on("click", function(event){
				event.preventDefault();
			
				$('#content .view-mode').find('.list').addClass('active');
				$('#content .view-mode').find('.grid').removeClass('active');
				
				$('#content .shop-products').addClass('list-view');
				$('#content .shop-products').removeClass('grid-view');
				
				$('#content .list-col4').addClass('col-xs-12 col-sm-6 col-lg-4');
				$('#content .list-col8').addClass('col-xs-12 col-sm-6 col-lg-8');
			});
		});
		
		// is sticky header
		if ($(".vg-sticky").length > 0) {
			var fixedSection = $('.vg-sticky');
			// sticky nav
			var headerHeight = fixedSection.outerHeight();
			var stickyNavTop = fixedSection.offset().top;
			var wpadminbar   = 0;
			if ($("#wpadminbar").length > 0) {
				wpadminbar = $('#wpadminbar').height();
			}
			fixedSection.addClass('animated');
			fixedSection.before('<div class="nav-placeholder"></div>');
			$('.nav-placeholder').height('inherit');
			//add class
			fixedSection.addClass('sticky-header-out');
			var stickyNav = function () {
				$(window).resize(function(event) {
					fixedSection.removeClass('sticky-header').addClass('sticky-header-out');
					$('.nav-placeholder').height('inherit');
					if ($("#wpadminbar").length > 0) {
						wpadminbar = $('#wpadminbar').height();
					}
				});
				if($(window).width() > 1024){
					var scrollTop = $(window).scrollTop();
					if (scrollTop > (stickyNavTop - wpadminbar)) {
						fixedSection.removeClass('sticky-header-out').addClass('sticky-header').css('top', wpadminbar);
						$('.nav-placeholder').height(headerHeight);
					} else {
						if (fixedSection.hasClass('sticky-header')) {
							fixedSection.removeClass('sticky-header').addClass('sticky-header-out').css('top', 0);
							$('.nav-placeholder').height('inherit');
						}
					}
				}
			};
			stickyNav();
			$(window).scroll(function () {
				stickyNav();
			});
			
		}
	
		// Add none loading when click to cart
		$('a.ajax_add_to_cart').addClass('no-preloader');
		$('a.compare').addClass('no-preloader');
		$('a.add_to_wishlist').addClass('no-preloader');
		$('.single-product-image .images a').addClass('no-preloader');
		
		// Add loading when click to any link
		$('body a').click(function() {
			var link   = $(this).attr('href');
			var loader = $(this).hasClass('no-preloader');
			if(!loader && typeof link !== "undefined" && link.toLowerCase().indexOf("http://wordpress.vinagecko.net/") >= 0) {
				jQuery('#pageloader').show();
			}
		});
		
		// Remove loading when click on loading 
		$('#pageloader').click(function() {
			$('#pageloader').fadeOut('slow');
		});
		
		$('.SlectBox').SumoSelect({ csvDispCount: 3, captionFormatAllSelected: "Yeah, OK, so everything." });
	});
	
	
	//Counter About Us
	$('.statistic').appear(function() {
		$('.timer').countTo({
			speed: 4000,
			refreshInterval: 60,
			formatter: function(value, options) {
				return value.toFixed(options.decimals);
			}
		});
	});
	
	// Quantity buttons
	$('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').addClass('buttons_added');
	
	$(document).on('click', '.plus, .minus', function() {
		// Get values
		var $qty		= $(this).closest('.quantity').find('.qty'),
			currentVal	= parseFloat($qty.val()),
			max			= '',
			min			= 1,
			step		= 1;

		// Format values
		if(! currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
		if(max === '' || max === 'NaN') max = '';
		if(min === '' || min === 'NaN') min = 0;
		if(step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;

		// Change the value
		if($(this).is('.plus')) {
			if(max &&(max == currentVal || currentVal > max)) {
				$qty.val(max);
			} else {
				$qty.val(currentVal + parseFloat(step));
			}
		} else {
			if(min &&(min == currentVal || currentVal < min)) {
				$qty.val(min);
			} else if(currentVal > 0) {
				$qty.val(currentVal - parseFloat(step));
			}
		}

		// Trigger change event
		$qty.trigger('change');
	});
	
	//Fix Mini Cart Safari
	$('.mini_cart_inner').on("hover", function(){
		$(".lazy").lazy();
		var minicart = $('.mcart-border');
		if(minicart.hasClass('active')){
			minicart.removeClass('active');
			minicart.css('visibility', 'hidden');
		}else{
			minicart.addClass('active');
			minicart.css('visibility', 'visible');
		}
	});
	
	/*To Top*/
	$(".to-top").hide();
	/* fade in #back-top */
	$(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 100) {
				$('.to-top').fadeIn();
			} else {
				$('.to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('.to-top').on("click", function() {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
	
	// HieuJa add Lazy Load
	$(".lazy").lazy();
})(jQuery);

jQuery(window).load(function($) {
	jQuery(".lazy").lazy();
	jQuery('#pageloader').fadeOut('slow');
});

/* Get Param value */
function vinageckogetParameterByName(name, string) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(string);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
