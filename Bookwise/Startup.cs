﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookWise.Startup))]
namespace BookWise
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
