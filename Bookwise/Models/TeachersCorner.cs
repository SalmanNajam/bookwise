//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookWise.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TeachersCorner
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string SchoolName { get; set; }
        public string SchoolAddress { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
