﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookWise.Models
{
    public class Helper
    {
        public static string GiveBookName(int id)
        {
            BooksNames bookname = new BooksNames();
            string name = "";
            using (BookWiseEntities dal = new BookWiseEntities())
            {
                bookname = (from b in dal.BooksNames where b.Id == id select b).FirstOrDefault();
                name = bookname.Name;
            }
            return name;
        }
    }
}